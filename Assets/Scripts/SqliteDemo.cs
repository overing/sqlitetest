﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UnityEngine;

public sealed class SqliteDemo : MonoBehaviour
{
    static readonly string ConnectionString = "URI=file:test.db";

    IDbConnection DbConnection;

    GUIStyle LabelStyle;
    GUIStyle FieldStyle;
    GUIStyle ButtonStyle;
    Vector2 ResultViewPosition;

    string InputFirstName = string.Empty;
    string InputLastName = string.Empty;
    string QueryResult = string.Empty;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static void AfterSceneLoad()
    {
        if (FindObjectOfType<SqliteDemo>() != null)
            return;

        FindObjectOfType<Camera>().clearFlags = CameraClearFlags.SolidColor;

        var type = typeof(SqliteDemo);
        new GameObject(type.Name, type);
    }

    void Start()
    {
        var conn = DbConnection = new Mono.Data.Sqlite.SqliteConnection(ConnectionString);
        conn.Open();
        conn.EnsureTestTable();
    }

    void OnGUI()
    {
        var conn = DbConnection;
        var labelStyle = LabelStyle ?? (LabelStyle = new GUIStyle(GUI.skin.label) { fontSize = Screen.height / 22 });
        var fieldStyle = FieldStyle ?? (FieldStyle = new GUIStyle(GUI.skin.textField) { fontSize = Screen.height / 24 });
        var buttonStyle = ButtonStyle ?? (ButtonStyle = new GUIStyle(GUI.skin.button) { fontSize = Screen.height / 24 });

        GUILayout.BeginVertical(GUI.skin.box);
        {
            if (GUILayout.Button("SELECT Data", buttonStyle))
                QueryResult = conn.SelectTestTable()
                    .Aggregate(new StringBuilder("Select Result: "), (buff, r) =>
                        buff.AppendLine().AppendFormat("Name: {0}, {1}", r.firstName, r.lastName))
                    .ToString();

            if (GUILayout.Button("DELETE Data", buttonStyle))
                QueryResult = "Delete Result: " + conn.ClearTestTable();
        }
        GUILayout.EndVertical();

        GUILayout.BeginVertical(GUI.skin.box);
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("First Name", labelStyle);
                InputFirstName = GUILayout.TextField(InputFirstName, fieldStyle, GUILayout.Width(Screen.width / 4));
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Last Name", labelStyle);
                InputLastName = GUILayout.TextField(InputLastName, fieldStyle, GUILayout.Width(Screen.width / 4));
            }
            GUILayout.EndHorizontal();

            if (GUILayout.Button("INSERT Data", buttonStyle))
            {
                if (string.IsNullOrEmpty(InputFirstName))
                    QueryResult = "Require first name.";
                else if (string.IsNullOrEmpty(InputLastName))
                    QueryResult = "Require last name.";
                else
                {
                    var result = conn.InsertTestTable(InputFirstName, InputLastName);
                    if (result > 0)
                    {
                        InputFirstName = string.Empty;
                        InputLastName = string.Empty;
                    }
                    QueryResult = "Insert Result: " + result;
                }
            }
        }
        GUILayout.EndVertical();

        GUILayout.Space(GUI.skin.label.lineHeight);
        ResultViewPosition = GUILayout.BeginScrollView(ResultViewPosition);
        {
            GUILayout.Label(QueryResult, labelStyle);
        }
        GUILayout.EndScrollView();
    }

    void OnDestroy() => DbConnection.Close();
}

/// <summary>
/// 為 <see cref="IDbConnection"/> 擴充跟 `test_table` 表格相關的方法
/// </summary>
static class DbConnectionTestTableExtensions
{
    static readonly string TableName = "test_table";
    static readonly string FirstNameColumn = "first_name";
    static readonly string LastNameColumn = "last_name";

    /// <summary>
    /// 確保 `test_table` 表格存在
    /// </summary>
    /// <param name="conn">要建立表格的 db</param>
    /// <returns>返回 SQL 操作結果</returns>
    public static int EnsureTestTable(this IDbConnection conn)
    {
        using (var cmd = conn.CreateCommand())
        {
            cmd.CommandText = $@"
CREATE TABLE IF NOT EXISTS `{TableName}` (
    `{FirstNameColumn}` TEXT,
    `{LastNameColumn}` TEXT
);
            ";
            return cmd.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// 查詢 `test_table` 表格的所有記錄
    /// </summary>
    /// <param name="conn">表格所在的 db</param>
    /// <returns>返回 SQL 查詢結果</returns>
    public static IEnumerable<(string firstName, string lastName)> SelectTestTable(this IDbConnection conn)
    {
        using (var cmd = conn.CreateCommand())
        {
            cmd.CommandText = $@"
SELECT `{FirstNameColumn}`, `{LastNameColumn}` 
FROM `{TableName}`;
            ";
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    yield return
                    (
                        reader.GetString(0),
                        reader.GetString(1)
                    );
                }
            }
        }
    }

    /// <summary>
    /// 插入新紀錄到 `test_table` 表格
    /// </summary>
    /// <param name="conn">表格所在的 db</param>
    /// <param name="firstName">名稱</param>
    /// <param name="lastName">姓氏</param>
    /// <returns>返回 SQL 操作結果 - 新增的資料筆數</returns>
    public static int InsertTestTable(this IDbConnection conn, string firstName, string lastName)
    {
        using (var cmd = conn.CreateCommand())
        {
            cmd.CommandText = $@"
INSERT INTO `test_table` (
    `{FirstNameColumn}`,
    `{LastNameColumn}`
) VALUES (
    @first_name,
    @last_name
);
            ";
            cmd.AddParameter("@first_name", firstName);
            cmd.AddParameter("@last_name", lastName);
            return cmd.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// 清除 `test_table` 表格的所有記錄
    /// </summary>
    /// <param name="conn">表格所在的 db</param>
    /// <returns>返回 SQL 操作結果 - 刪除的資料筆數</returns>
    public static int ClearTestTable(this IDbConnection conn)
    {
        using (var cmd = conn.CreateCommand())
        {
            cmd.CommandText = $@"
DELETE FROM `{TableName}` WHERE 1;
            ";
            return cmd.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// 追加參數到 <see cref="IDbCommand"/>
    /// </summary>
    /// <param name="cmd">目標 Command</param>
    /// <param name="name">參數名稱</param>
    /// <param name="value">參數值</param>
    static void AddParameter(this IDbCommand cmd, string name, object value)
    {
        var param = cmd.CreateParameter();
        param.ParameterName = name;
        param.Value = value;
        cmd.Parameters.Add(param);
    }
}